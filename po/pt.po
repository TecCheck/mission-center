# Portuguese translations for missioncenter.
# Copyright (C) 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the missioncenter package.
# Rilson Joás, 2023.
# Rafael Fontenelle <rafaelff@gnome.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: missioncenter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-06 16:25+0300\n"
"PO-Revision-Date: 2023-07-13 00:39-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Portuguese\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 45.alpha0\n"

#: data/io.missioncenter.MissionCenter.desktop.in:3
msgid "Mission Center"
msgstr "Mission Center"

#: data/io.missioncenter.MissionCenter.appdata.xml.in:7
msgid "No description"
msgstr "Sem descrição"

#: data/io.missioncenter.MissionCenter.gschema.xml:14
msgid "Which page is shown on application startup"
msgstr "Qual página deve ser exibida na inicialização do aplicativo"

#: data/io.missioncenter.MissionCenter.gschema.xml:20
msgid "How fast should the data be refreshed and the UI updated"
msgstr ""
"Com que velocidade os dados devem ser atualizados e a interface do usuário "
"deve ser atualizada"

#: data/io.missioncenter.MissionCenter.gschema.xml:26
msgid "Which graph is shown on the CPU performance page"
msgstr "Qual gráfico é mostrado na página de desempenho da CPU"

#: data/io.missioncenter.MissionCenter.gschema.xml:31
msgid "Which page is shown on application startup, in the performance tab"
msgstr ""
"Qual página é mostrada na inicialização do aplicativo, na aba de Desempenho"

#: resources/ui/performance_page/cpu.blp:42 src/performance_page/mod.rs:267
msgid "CPU"
msgstr "CPU"

#: resources/ui/performance_page/cpu.blp:74
msgid "% Utilization"
msgstr "% de utilização"

#: resources/ui/performance_page/cpu.blp:84
msgid "100%"
msgstr "100%"

#: resources/ui/performance_page/cpu.blp:152
#: resources/ui/performance_page/gpu.blp:224
msgid "Utilization"
msgstr "Utilização"

#: resources/ui/performance_page/cpu.blp:174
msgid "Speed"
msgstr "Velocidade"

#: resources/ui/performance_page/cpu.blp:201
msgid "Processes"
msgstr "Processos"

#: resources/ui/performance_page/cpu.blp:223
msgid "Threads"
msgstr "Threads"

#: resources/ui/performance_page/cpu.blp:245
msgid "Handles"
msgstr "Identificadores"

#: resources/ui/performance_page/cpu.blp:268
msgid "Up time"
msgstr "Tempo de atividade"

#: resources/ui/performance_page/cpu.blp:294
msgid "Base Speed:"
msgstr "Velocidade base:"

#: resources/ui/performance_page/cpu.blp:303
msgid "Sockets:"
msgstr "Sockets:"

#: resources/ui/performance_page/cpu.blp:312
msgid "Virtual processors:"
msgstr "Processadores virtuais:"

#: resources/ui/performance_page/cpu.blp:321
msgid "Virtualization:"
msgstr "Virtualização:"

#: resources/ui/performance_page/cpu.blp:330
msgid "Virtual machine:"
msgstr "Máquina virtual:"

#: resources/ui/performance_page/cpu.blp:339
msgid "L1 cache:"
msgstr "Cache L1:"

#: resources/ui/performance_page/cpu.blp:348
msgid "L2 cache:"
msgstr "Cache L2:"

#: resources/ui/performance_page/cpu.blp:357
msgid "L3 cache:"
msgstr "Cache L3:"

#: resources/ui/performance_page/cpu.blp:441
msgid "Change G_raph To"
msgstr "Mudar g_ráfico para"

#: resources/ui/performance_page/cpu.blp:444
msgid "Overall U_tilization"
msgstr "U_tilização geral"

#: resources/ui/performance_page/cpu.blp:449
msgid "Logical _Processors"
msgstr "_Processadores lógicos"

#: resources/ui/performance_page/cpu.blp:457
#: resources/ui/performance_page/disk.blp:385
#: resources/ui/performance_page/gpu.blp:453
#: resources/ui/performance_page/memory.blp:401
#: resources/ui/performance_page/network.blp:368
msgid "Graph _Summary View"
msgstr "Visão re_sumida"

#: resources/ui/performance_page/cpu.blp:462
#: resources/ui/performance_page/disk.blp:390
#: resources/ui/performance_page/gpu.blp:458
#: resources/ui/performance_page/memory.blp:406
#: resources/ui/performance_page/network.blp:373
msgid "_View"
msgstr "_Visão"

#: resources/ui/performance_page/cpu.blp:465
#: resources/ui/performance_page/disk.blp:393
#: resources/ui/performance_page/gpu.blp:461
#: resources/ui/performance_page/memory.blp:409
#: resources/ui/performance_page/network.blp:376
msgid "CP_U"
msgstr "CP_U"

#: resources/ui/performance_page/cpu.blp:470
#: resources/ui/performance_page/disk.blp:398
#: resources/ui/performance_page/gpu.blp:466
#: resources/ui/performance_page/memory.blp:414
#: resources/ui/performance_page/network.blp:381
msgid "_Memory"
msgstr "_Memória"

#: resources/ui/performance_page/cpu.blp:475
#: resources/ui/performance_page/disk.blp:403
#: resources/ui/performance_page/gpu.blp:471
#: resources/ui/performance_page/memory.blp:419
#: resources/ui/performance_page/network.blp:386
msgid "_Disk"
msgstr "_Disco"

#: resources/ui/performance_page/cpu.blp:480
#: resources/ui/performance_page/disk.blp:408
#: resources/ui/performance_page/gpu.blp:476
#: resources/ui/performance_page/memory.blp:424
#: resources/ui/performance_page/network.blp:391
msgid "_Network"
msgstr "R_ede"

#: resources/ui/performance_page/cpu.blp:485
#: resources/ui/performance_page/disk.blp:413
#: resources/ui/performance_page/gpu.blp:481
#: resources/ui/performance_page/memory.blp:429
#: resources/ui/performance_page/network.blp:396
msgid "_GPU"
msgstr "_GPU"

#: resources/ui/performance_page/cpu.blp:493
#: resources/ui/performance_page/disk.blp:421
#: resources/ui/performance_page/gpu.blp:489
#: resources/ui/performance_page/memory.blp:437
#: resources/ui/performance_page/network.blp:409
msgid "_Copy"
msgstr "_Cópia"

#: resources/ui/performance_page/disk.blp:64
#: resources/ui/performance_page/disk.blp:188
msgid "Active time"
msgstr "Tempo de atividade"

#: resources/ui/performance_page/disk.blp:122
msgid "Disk transfer rate"
msgstr "Taxa de transferência de disco"

#: resources/ui/performance_page/disk.blp:211
msgid "Average response time"
msgstr "Tempo médio de resposta"

#: resources/ui/performance_page/disk.blp:247
msgid "Read speed"
msgstr "Velocidade de leitura"

#: resources/ui/performance_page/disk.blp:279
msgid "Write speed"
msgstr "Velocidade de escrita"

#: resources/ui/performance_page/disk.blp:307
msgid "Capacity:"
msgstr "Capacidade:"

#: resources/ui/performance_page/disk.blp:316
msgid "Formatted:"
msgstr "Formatado:"

#: resources/ui/performance_page/disk.blp:325
msgid "System disk:"
msgstr "Disco do sistema:"

#: resources/ui/performance_page/disk.blp:334
#: resources/ui/performance_page/memory.blp:345
msgid "Type:"
msgstr "Tipo:"

#: resources/ui/performance_page/gpu.blp:71
msgid "Overall utilization"
msgstr "Utilização geral"

#: resources/ui/performance_page/gpu.blp:110
msgid "Video encode"
msgstr "Codificação de vídeo"

#: resources/ui/performance_page/gpu.blp:143
msgid "Video decode"
msgstr "Decodificação de vídeo"

#: resources/ui/performance_page/gpu.blp:180
#: resources/ui/performance_page/gpu.blp:247
#: resources/ui/performance_page/memory.blp:73
msgid "Memory usage"
msgstr "Uso de memória"

#: resources/ui/performance_page/gpu.blp:275
msgid "Clock Speed"
msgstr "Velocidade do clock"

#: resources/ui/performance_page/gpu.blp:298
msgid "Memory speed"
msgstr "Velocidade de memória"

#: resources/ui/performance_page/gpu.blp:325
msgid "Power draw"
msgstr "Uso de poder"

#: resources/ui/performance_page/gpu.blp:348
msgid "Temperature"
msgstr "Temperatura"

#: resources/ui/performance_page/gpu.blp:375
msgid "OpenGL version:"
msgstr "Versão do OpenGL:"

#: resources/ui/performance_page/gpu.blp:384
msgid "Vulkan version:"
msgstr "Versão do Vulkan:"

#: resources/ui/performance_page/gpu.blp:393
msgid "PCI Express speed:"
msgstr "Velocidade de PCI Express:"

#: resources/ui/performance_page/gpu.blp:402
msgid "PCI bus address:"
msgstr "Endereço do PCI bus:"

#: resources/ui/performance_page/memory.blp:28
msgid "Some information requires administrative privileges"
msgstr "Algumas informações precisam de privilégios administrativos"

#: resources/ui/performance_page/memory.blp:29
msgid "_Authenticate"
msgstr "_Autenticar"

#: resources/ui/performance_page/memory.blp:52 src/performance_page/mod.rs:318
msgid "Memory"
msgstr "Memória"

#: resources/ui/performance_page/memory.blp:130
msgid "Memory composition"
msgstr "Composição da memória"

#: resources/ui/performance_page/memory.blp:166
msgid "In use"
msgstr "Em uso"

#: resources/ui/performance_page/memory.blp:189
msgid "Available"
msgstr "Disponível"

#: resources/ui/performance_page/memory.blp:217
msgid "Committed"
msgstr "Comprometido"

#: resources/ui/performance_page/memory.blp:240
msgid "Cached"
msgstr "Em cache"

#: resources/ui/performance_page/memory.blp:267
msgid "Swap available"
msgstr "Swap disponível"

#: resources/ui/performance_page/memory.blp:290
msgid "Swap used"
msgstr "Swap em uso"

#: resources/ui/performance_page/memory.blp:318
msgid "Speed:"
msgstr "Velocidade:"

#: resources/ui/performance_page/memory.blp:327
msgid "Slots used:"
msgstr "Slots usados:"

#: resources/ui/performance_page/memory.blp:336
msgid "Form factor:"
msgstr "Formato de memória:"

#: resources/ui/performance_page/network.blp:62
msgid "Throughput"
msgstr "Taxa de transferência"

#: resources/ui/performance_page/network.blp:139
msgid "Send"
msgstr "Enviado"

#: resources/ui/performance_page/network.blp:170
msgid "Receive"
msgstr "Recebido"

#: resources/ui/performance_page/network.blp:197
msgid "Interface name:"
msgstr "Nome do adaptador:"

#: resources/ui/performance_page/network.blp:206
msgid "Connection type:"
msgstr "Tipo de conexão:"

#: resources/ui/performance_page/network.blp:216
msgid "SSID:"
msgstr "SSID:"

#: resources/ui/performance_page/network.blp:226
msgid "Signal strength:"
msgstr "Força do sinal:"

#: resources/ui/performance_page/network.blp:236
msgid "Maximum Bitrate:"
msgstr "Fluxo máximo:"

#: resources/ui/performance_page/network.blp:246
msgid "Frequency:"
msgstr "Frequência:"

#: resources/ui/performance_page/network.blp:255
msgid "Hardware address:"
msgstr "Endereço local:"

#: resources/ui/performance_page/network.blp:264
msgid "IPv4 address:"
msgstr "Endereço IPv4:"

#: resources/ui/performance_page/network.blp:273
msgid "IPv6 address:"
msgstr "Endereço IPv6:"

#: resources/ui/performance_page/network.blp:402
msgid "Network Se_ttings"
msgstr "C_onfigurações de rede"

#: resources/ui/preferences/page.blp:6
msgid "General Settings"
msgstr "Configurações gerais"

#: resources/ui/preferences/page.blp:9
msgid "Update Speed"
msgstr "Velocidade de atualização"

#: resources/ui/preferences/page.blp:12 src/preferences/page.rs:90
#: src/preferences/page.rs:227
msgid "Very Slow"
msgstr "Muito lenta"

#: resources/ui/preferences/page.blp:13
msgid "Refresh every 2 seconds"
msgstr "Atualizar a cada 2 segundos"

#: resources/ui/preferences/page.blp:17 src/preferences/page.rs:94
#: src/preferences/page.rs:223
msgid "Slow"
msgstr "Lenta"

#: resources/ui/preferences/page.blp:18
msgid "Refresh every second and a half"
msgstr "Atualizar a cada segundo e meio"

#: resources/ui/preferences/page.blp:22 src/preferences/page.rs:98
#: src/preferences/page.rs:111 src/preferences/page.rs:219
#: src/preferences/page.rs:235
msgid "Normal"
msgstr "Normal"

#: resources/ui/preferences/page.blp:23
msgid "Refresh every second"
msgstr "Atualizar a cada segundo"

#: resources/ui/preferences/page.blp:27 src/preferences/page.rs:102
#: src/preferences/page.rs:215
msgid "Fast"
msgstr "Rápida"

#: resources/ui/preferences/page.blp:28
msgid "Refresh every half second"
msgstr "Atualizar a cada meio segundo"

#: resources/ui/window.blp:48
msgid "Type a name or PID to search"
msgstr "Digite um nome ou identificador para iniciar busca"

#: resources/ui/window.blp:80
msgid "Performance"
msgstr "Desempenho"

#: resources/ui/window.blp:89
msgid "Apps"
msgstr "Aplicativos"

#: resources/ui/window.blp:101
msgid "_Preferences"
msgstr "_Preferências"

#: resources/ui/window.blp:106
msgid "_About MissionCenter"
msgstr "_Sobre o Mission Center"

#: resources/ui/window.blp:113
msgid "_Quit"
msgstr "_Sair"

#: src/performance_page/widgets/mem_composition_widget.rs:224
msgid ""
"In use ({}iB)\n"
"\n"
"Memory used by the operating system and running applications"
msgstr ""
"Em uso ({}iB)\n"
"\n"
"Memória usada pelo sistema operacional e pelos aplicativos"

#: src/performance_page/widgets/mem_composition_widget.rs:231
msgid ""
"Modified ({}iB)\n"
"\n"
"Memory whose contents must be written to disk before it can be used by "
"another process"
msgstr ""
"Modificado ({}iB)\n"
"\n"
"Conteúdo de memória que precisa ser escrito ao disco antes de ser usado por "
"outro processo"

#: src/performance_page/widgets/mem_composition_widget.rs:252
msgid ""
"Standby ({}iB)\n"
"\n"
"Memory that contains cached data and code that is not actively in use"
msgstr ""
"Em espera ({}iB)\n"
"\n"
"Memória que contém dados em cache e código ainda não ativo"

#: src/performance_page/widgets/mem_composition_widget.rs:261
msgid ""
"Free ({}iB)\n"
"\n"
"Memory that is not currently in use, and that will be repurposed first when "
"the operating system, drivers, or applications need more memory"
msgstr ""
"Livre ({}iB)\n"
"\n"
"Memória que não se encontra em uso, e que vai ser ter prioridade quando o "
"sistema, os drivers ou aplicativos precisem de mais memória"

#: src/performance_page/mod.rs:371 src/performance_page/disk.rs:195
msgid "Disk {} ({})"
msgstr "Disco {} ({})"

#: src/performance_page/mod.rs:375
msgid "HDD"
msgstr "HDD"

#: src/performance_page/mod.rs:376
msgid "SSD"
msgstr "SSD"

#: src/performance_page/mod.rs:377
msgid "NVMe"
msgstr "NVMe"

#: src/performance_page/mod.rs:378
msgid "eMMC"
msgstr "eMMC"

#: src/performance_page/mod.rs:379
msgid "iSCSI"
msgstr "iSCSI"

#: src/performance_page/mod.rs:380 src/performance_page/cpu.rs:278
#: src/performance_page/cpu.rs:291 src/performance_page/cpu.rs:301
#: src/performance_page/cpu.rs:307 src/performance_page/gpu.rs:239
#: src/performance_page/memory.rs:309 src/performance_page/network.rs:375
#: src/performance_page/network.rs:396 src/performance_page/network.rs:404
#: src/performance_page/network.rs:441 src/performance_page/network.rs:519
msgid "Unknown"
msgstr "Desconhecido"

#: src/performance_page/mod.rs:450 src/performance_page/network.rs:331
msgid "Ethernet"
msgstr "Ethernet"

#: src/performance_page/mod.rs:451 src/performance_page/network.rs:338
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/performance_page/mod.rs:452 src/performance_page/network.rs:340
msgid "Other"
msgstr "Outro"

#: src/performance_page/mod.rs:541
msgid "GPU {}"
msgstr "GPU {}"

#: src/performance_page/mod.rs:695
msgid "{}: {} {}bps {}: {} {}bps"
msgstr "{}: {} {}bps {}: {} {}bps"

#: src/performance_page/cpu.rs:286
msgid "Supported"
msgstr "Compatível"

#: src/performance_page/cpu.rs:288 src/performance_page/gpu.rs:250
msgid "Unsupported"
msgstr "Não compatível"

#: src/performance_page/cpu.rs:296 src/performance_page/disk.rs:215
msgid "Yes"
msgstr "Sim"

#: src/performance_page/cpu.rs:298 src/performance_page/disk.rs:217
msgid "No"
msgstr "Não"

#: src/performance_page/cpu.rs:635
msgid "% Utilization over {} seconds"
msgstr "% de uso em {} segundos"

#: src/performance_page/cpu.rs:639 src/performance_page/disk.rs:368
#: src/performance_page/memory.rs:372 src/performance_page/network.rs:605
msgid "{} seconds"
msgstr "{} segundos"

#: src/performance_page/disk.rs:243
msgid "{} {}{}B/s"
msgstr "{} {}{}B/s"

#: src/performance_page/network.rs:415 src/performance_page/network.rs:421
#: src/performance_page/network.rs:429
msgid "{} {}bps"
msgstr "{} {}bps"

#: src/performance_page/network.rs:454 src/performance_page/network.rs:470
msgid "N/A"
msgstr "N/A"
